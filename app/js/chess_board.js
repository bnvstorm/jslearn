$(document).ready(function () {
    for (var i = 0; i < 8; i++) {
        for (var j = 0; j < 8; j++) {
            if ((i % 2 == 0 && j % 2 == 0) || (i % 2 != 0 && j % 2 != 0)) {
                $("#chess_board").append('<div class="square w"></div>');
            }
            else $("#chess_board").append('<div class="square b"></div>');
        }
    }
});