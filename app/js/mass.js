//МАССИВЫ\\
var m = [],
    p = document.getElementById('out'),
    dele = document.getElementById('dele'),
    str = '',
    nstr;

function push() {
    var input = document.getElementById('input').value,
        str = '',
        a;
        
    if (input == 0) {
        p.innerHTML = 'Вы ничего не ввели!';
    } else {
        p.classList.add('accept');
        m.push(input);
        view();
    }
}

function view() {
    if (m.length == 0) {
        if (p.classList.contains("accept") == true) {
            p.classList.remove("accept");
        }
        p.classList.add("warning");
        p.innerHTML = 'Массив пустой!';

    } else {
        if (p.classList.contains("warning") == true) {
            p.classList.remove("warning");
        }
        for (i = 0; i < m.length; i++) {

            str += (i + 1) + ' ---- ' + m[i] + '<br>';
        }
        nstr = str;
        str = '';
        p.innerHTML = nstr;
        dele.classList.add('block');
    }

}

function clr() {
    p.innerHTML = 'Поле очищено!';
    document.getElementById('input').value ='';
}

function del() {
    m = [];
    p.classList.remove("accept");
    p.classList.add("warning");
    dele.classList.remove('block');
    p.innerHTML = 'Массив пустой!';
}

function rem() {
    var a, elem, delet;
    delet = document.getElementById('delet').value;
    for (i = 0; i < m.length; i++) {
        elem = m[i];
        a = i;
        // alert(m[i]);
        if (a == delet) {
            m.splice(a-1, 1);
            view();
        }
    }
}

view();