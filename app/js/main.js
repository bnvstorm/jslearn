//УГАДАЙ ЧИСЛО\\
var oldNum, genNum, newOut, oldOut, num, out, empty, card;
genNum = Math.floor((Math.random() * 10) + 1); //генерим число
oldNum = genNum;

function guess() {

    //сохраняем его в переменную

    num = document.getElementById('user-num').value; //присваеваем переменной число из инпута с id userNum
    out = document.getElementById('outTxt'); //присваеваем outTxt в out

    if (num == genNum) {
        out.innerHTML = 'Попал!';
    } else if (num > genNum) {
        out.innerHTML = 'Больше чем нужно!';
    } else if (num < genNum) {
        out.innerHTML = 'Меньше чем нужно!';
    }
    else{
        out.innerHTML = 'ERROR';
    }

}

function restart() {

    genNum = Math.floor((Math.random() * 10) + 1);
    card = document.getElementById('alertCard');
    card.className += " block";
    card.insertAdjacentHTML('beforeEnd', '<p id="temp-out"></p>');
    //очистка значений

    empty = document.getElementById('temp-out');
    empty.innerHTML = ' ';
    // empty = document.getElementById('new-temp-out');
    // empty.innerHTML = ' ';

    oldOut = document.getElementById('temp-out');
    oldOut.insertAdjacentHTML('beforeEnd', 'Загаданное число <br> было равно <br><br> ' + oldNum + '<br><br>  обновите страницу для генерации нового числа! ');


}
// //ЦИКЛЫ\\
// function f2() {
//     p = document.getElementById('out');
//     for (var i = 0; i < 100; i++) {
//         p.innerHTML += " " + i;
//     }
// }

// function f3() {
//     p = document.getElementById('out');
//     var i = 0;
//     while (i < 50) {
//         i++;
//         p.innerHTML += i + ' ';

//     }
//     var j = 0;
//     do {
//         j++;
//         p.innerHTML += j + ' ';
//     }
//     while (j < 30);

// }
